<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory;

    protected $fillable=[
        'name',
        'description',
        'category_id',
        'sub_cat_id',
        'page_link',
        'no_of_followers',
        'website_url',
        'tag_id',
        'email',
        'phone',

    ];
}
