<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->id();
            $table->string('name',100)->unique();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->unsignedBigInteger('sub_cat_id')->nullable();
            $table->longText('description')->nullable();
            $table->string('img')->nullable();
            $table->string('page_link');
            $table->integer('no_of_followers');
            $table->integer('rating');
            $table->string('website_url')->nullable();
            $table->unsignedBigInteger('tag_id')->nullable();
            $table->string('email')->nullable()->unique();
            $table->string('phone')->nullable();
            // $table->foreign('category_id')->references('id')->on('categories')->nullOnDelete();
            // $table->foreign('sub_cat_id')->references('id')->on('sub_categories')->nullOnDelete();
            // $table->foreign('tag_id')->references('id')->on('tags')->nullOnDelete();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pages');
    }
};
