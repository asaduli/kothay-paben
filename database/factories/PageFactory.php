<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Page>
 */
class PageFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            //

            'name' =>fake()->company(),
            'description' =>fake()->sentence($nbWords = 6, $variableNbWords = true),
            'category_id' =>fake()->randomDigitNotNull(),
            'sub_cat_id' =>fake()->randomDigitNotNull(),
            'page_link' =>fake()->url(),
            'no_of_followers' =>fake()->numberBetween($min=10,$max=1000),
            'rating' =>fake()->randomDigitNotNull(),
            'website_url' =>fake()->domainName(),
            'tag_id'=>fake()->randomDigitNotNull(),
            'email' =>fake()->safeEmail(),
            'phone' =>fake()->phoneNumber(),
        ];
    }
}
