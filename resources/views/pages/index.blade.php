{{-- 
@extends('layout')
 
@section('content')

@section('hero')

@include('partials._hero')
@endsection

@include('partials._nav')
@include('partials._search') --}}

<x-layout>

    <x-partials._nav>
        <x-partials._hero></x-partials._hero>
    </x-partials._nav>


    <section class="product spad">
        <div class="container">
            <div class="row property__gallery justify-content-center">

                @foreach ($pages as $page)
                    <div class="col-sm-4">
                        <div class="product__item">
                            <x-paging-card>
                                <a href="/page/{{ $page->id }}"><img class="card-img-top"
                                        src="{{ asset('images/Logo.JPG') }}" alt="Card image cap"></a>
                                <div class="card-body">
                                    <p class="card-text">{{ $page->description }}</p>
                                </div>
                            </x-paging-card>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>


</x-layout>
