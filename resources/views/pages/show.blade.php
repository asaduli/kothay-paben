<x-layout>

    <x-partials._nav></x-partials._nav>
    <x-paging-card>
  
        <x-card class="mx-auto bg-black">
            <a href="/page/{{ $page->id }}"><img class="card-img-top" src="{{ asset('images/Logo.JPG') }}"
                alt="Card image cap"></a>
        <div class="card-body">
            <p class="card-text">{{ $page->description }}</p>
        </div>

        </x-card>
    </x-paging-card>
</x-layout>

