<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


 Route::get('/data',function(){

    return response()->json(
        [
            'users1' =>[
                'name'=>'Asadul Islam',
                'city'=>'Barishal'
            ],
            'users2' =>[
                'name'=>'Dayna Mirdha',
                'city'=>'Patuakhali'
            ]
        
        ]
    );
 });

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
