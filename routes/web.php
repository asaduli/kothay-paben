<?php

use App\Http\Controllers\PageController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/',[PageController::class,'index']);

Route::get('/hello', function () {

    return response('<h1>Hello world</h1>', 200)
        ->header('Content-Type', 'text/plain')
        ->header('foo', 'bar');
});


Route::get('/page/{page}',[PageController::class,'show']) ;



Route::get('/search', function (Request $request) {
    return ($request->name . ' ' . $request->city);
});
